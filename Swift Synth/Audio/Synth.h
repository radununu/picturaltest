//
//  Synth.h
//  Swift Synth
//
//  Created by Nunu Radu on 25.11.19.
//  Copyright © 2019 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef float (^Signal)(float);

@interface Synth : NSObject
@property(nonatomic) float volume;
@property(class, readonly) Synth *shared;

- (void)setWaveFormTo:(Signal)signal;
- (BOOL)playerIsPlaying;

@end

NS_ASSUME_NONNULL_END

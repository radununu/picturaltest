//
//  Synth.m
//  Swift Synth
//
//  Created by Nunu Radu on 25.11.19.
//  Copyright © 2019 Grant Emerson. All rights reserved.
//

#import "Synth.h"
#import <AVFoundation/AVFoundation.h>
#import "Oscillator.h"
#import "Synth.h"

@interface Synth ()
@property (nonatomic, strong) AVAudioEngine *audioEngine;
@property (nonatomic) float time;
@property (nonatomic) double sampleRate;
@property (nonatomic) float deltaTime;
@property (nonatomic) Signal signal;
@property (nonatomic, strong) AVAudioSourceNode *sourceNode;

@end

@implementation Synth
@synthesize volume = _volume;

static Synth *sharedInstance;

+ (Synth *)shared {
    if (sharedInstance == nil) {
        sharedInstance = [[Synth alloc] init];
    }
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.audioEngine = [[AVAudioEngine alloc]init];
        AVAudioMixerNode *mainMixer = self.audioEngine.mainMixerNode;
        AVAudioOutputNode *outputNode = self.audioEngine.outputNode;
        AVAudioFormat *format = [outputNode inputFormatForBus:0];
        self.sampleRate = format.sampleRate;
        self.deltaTime = 1 / (float)self.sampleRate;
        self.signal = [Oscillator sine];
        
        AVAudioFormat *inputFormat = [[AVAudioFormat alloc]initWithCommonFormat:format.commonFormat sampleRate:self.sampleRate channels:1 interleaved:format.isInterleaved];
        [self.audioEngine attachNode:self.sourceNode];
        [self.audioEngine connect:self.sourceNode to:mainMixer format:inputFormat];
        [self.audioEngine connect:mainMixer to:outputNode format:nil];
        mainMixer.outputVolume = 0;
        
        @try {
            NSError *error = nil;
            [self.audioEngine startAndReturnError:&error];
        }
        @catch (NSError *throwError) {
           NSLog(@"Could not start audioEngine: %@", throwError);
        }
    }
    return self;
}

- (AVAudioSourceNode *)sourceNode {
    if (_sourceNode == nil) {
        _sourceNode = [[AVAudioSourceNode alloc] initWithRenderBlock:^OSStatus(BOOL * _Nonnull isSilence, const AudioTimeStamp * _Nonnull timestamp, AVAudioFrameCount frameCount, AudioBufferList * _Nonnull outputData) {
            
            for (int frame = 0; frame <= (int)frameCount; frame++) {
                float sampleVal = self.signal(self.time);
                self.time += self.deltaTime;
                
                for (int i = 0; i < outputData->mNumberBuffers; i++) {
                    float *buffer = outputData->mBuffers[i].mData;
                    buffer[frame] = sampleVal;
                }
            }
            return noErr;
            
        }];
    }
    return _sourceNode;
}

- (float)volume {
    return [self.audioEngine mainMixerNode].outputVolume;
}

- (void)setVolume:(float)volume {
    [[self.audioEngine mainMixerNode] setOutputVolume:volume];
}

- (void)setWaveFormTo:(Signal)signal {
    self.signal = signal;
}

- (BOOL)playerIsPlaying {
    return self.volume > 0;
}

@end

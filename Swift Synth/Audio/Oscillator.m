//
//  Oscillator.m
//  Swift Synth
//
//  Created by Nunu Radu on 25.11.19.
//  Copyright © 2019 Grant Emerson. All rights reserved.
//

#import "Oscillator.h"

@implementation Oscillator
static float _amplitude = 1;
static float _frequency = 440;

+ (float)amplitude {
    return _amplitude;
}

+ (void)setAmplitude:(float)amplitude {
    _amplitude = amplitude;
}

+ (float)frequency {
    return _frequency;
}

+ (void)setFrequency:(float)frequency {
    _frequency = frequency;
}

+ (CompletionBlock)triangle {
    return ^(float time){
        double period = 1.0 / (double)[self frequency];
        double currentTime = fmod((double)time, period);
        double value = currentTime / period;
        double result = 0.0;
        
        if (value < 0.25) {
            result = value * 4;
        } else if (value < 0.75) {
            result = 2.0 - (value * 4.0);
        } else {
            result = value * 4 - 4.0;
        }
        
        return [self amplitude] * (float)result;
    };
}

+ (CompletionBlock)sine {
    return ^(float time){
        return [self amplitude] * sinf(2.0 * M_PI * [self frequency] * time);
    };
}

+ (CompletionBlock)sawtooth {
    return ^(float time){
        double period = 1.0 / (double)[self frequency];
        double currentTime = fmod((double)time, period);
        return [self amplitude] * (float)((currentTime / period) * 2 - 1.0);
    };
}

+ (CompletionBlock)square {
    return ^(float time){
        double period = 1.0 / (double)[self frequency];
        double currentTime = fmod((double)time, period);
        return (float)(((currentTime / period) < 0.5) ? [self amplitude] : -1.0 * [self amplitude]);
    };
}

+ (CompletionBlock)whiteNoise {
    return ^(float time){
        return (float)[self amplitude] * (float)[Oscillator generateRandomNumberWithlowerBound:-1 upperBound:1];
    };
}

+ (int)generateRandomNumberWithlowerBound:(int)lowerBound
                               upperBound:(int)upperBound {
    return lowerBound + arc4random() % (upperBound - lowerBound);
}

@end

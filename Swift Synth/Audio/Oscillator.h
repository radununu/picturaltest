//
//  Oscillator.h
//  Swift Synth
//
//  Created by Nunu Radu on 25.11.19.
//  Copyright © 2019 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, WaveForm)
{
    sine,
    triangle,
    sawtooth,
    square,
    whiteNoise
};

typedef float (^CompletionBlock)(float);

@interface Oscillator : NSObject
@property(class) float amplitude;
@property(class) float frequency;

+ (CompletionBlock)triangle;
+ (CompletionBlock)sawtooth;
+ (CompletionBlock)square;
+ (CompletionBlock)whiteNoise;
+ (CompletionBlock)sine;

@end

NS_ASSUME_NONNULL_END
